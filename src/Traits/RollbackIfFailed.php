<?php

namespace Drupal\rollback\Traits;

/**
 * Trait RollbackIfFailed.
 *
 * Used to signify an update should be rolled back if an error is caught.
 */
trait RollbackIfFailed {
}
